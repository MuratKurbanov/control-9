import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://control-9-js.firebaseio.com/'
});

export default instance;