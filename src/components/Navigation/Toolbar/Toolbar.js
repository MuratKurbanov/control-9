import React from 'react';
import NavigationPages from '../NavigationPages/NavigationPages';

const Toolbar = () => {
  return (
    <header className='Toolbar'>
      <h1>Contacts</h1>
      <nav>
        <NavigationPages />
      </nav>
    </header>
  )
};

export default Toolbar;