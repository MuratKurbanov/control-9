import React from 'react';
import {NavLink} from "react-router-dom";

const NavigationPage = ({to, exact, children}) => {
  return (
    <li className='NavigationPage'>
      <NavLink to={to} exact>
        {children}
      </NavLink>
    </li>
  );
};

export default NavigationPage;