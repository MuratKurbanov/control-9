import React from 'react';
import NavigationPage from "../NavigationPage/NavigationPage";

const NavigationPages = () => {
  return (
    <ul className='NavigationPages'>
      <NavigationPage to='/addNewContact' exact>Add new Contact</NavigationPage>
    </ul>
  );
};

export default NavigationPages;