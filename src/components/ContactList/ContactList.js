import React, {Component} from 'react';
import {deleteContact, getContacts} from "../../store/actions/actions";
import connect from "react-redux/es/connect/connect";


class ContactList extends Component {

  componentDidMount() {
    this.props.getContacts().then(() => {
    })
  };

  editContact = (id) => {
    this.props.history.push(`/editContact/${id}`)
  };

  render() {
    return (
      <div className="Contacts">
        {this.props.contacts.map((contact) => {
          return (
            <div className="Contact" key={contact.id}>
              <img src={contact.photo} alt="image" />
              <h2>{contact.name}</h2>
              <p>{contact.phone}</p>
              <p>{contact.email}</p>
              <button onClick={() => this.editContact(contact.id)}>edit</button>
              <button onClick={() => this.props.deleteContact(contact.id)}>delete</button>
            </div>
          )
        })}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    contacts: state.contacts
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getContacts: () => dispatch(getContacts()),
    deleteContact: (id) => dispatch(deleteContact(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);