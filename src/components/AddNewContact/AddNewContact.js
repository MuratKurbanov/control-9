import React, {Component} from 'react';
import {connect} from "react-redux";
import {postRequest} from "../../store/actions/actions";

class AddNewContact extends Component {
  state = {
    name: '',
    phone: '',
    email: '',
    photo: ''
  };

  valueChanged = event => {
    const name = event.target.name;
    this.setState({[name]: event.target.value});
  };

  contactsHandler = () => {
    const contacts = {...this.state};

    this.props.addContacts(contacts);
    this.props.history.push('/contactList')
  };

  backContact = () => {
    this.props.history.push('/')
  };

  render() {
    return (
      <div>
        <div>
          <h1>Add new Contact</h1>
        </div>
        <div>
          <label htmlFor="Name">Name: </label>
          <input type="text" name='name' id='Name'
                 value={this.state.name}
                 onChange={this.valueChanged}/>
          <label htmlFor="Phone">Phone: </label>
          <input type="text" name='phone' id='phone'
                 value={this.state.phone}
                 onChange={this.valueChanged}/>
          <label htmlFor="Email">Email: </label>
          <input type="email" name='email' id='Email'
                 value={this.state.email}
                 onChange={this.valueChanged}/>
          <label htmlFor="Photo">Photo URL: </label>
          <input type="text" name='photo' id='Photo'
                 value={this.state.photo}
                 onChange={this.valueChanged}/>
          <label htmlFor="PhotoPreview">Photo Preview: </label>
          <button onClick={this.contactsHandler}>Save</button>
          <button onClick={this.backContact}>Back to contacts</button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.loading
  }
};

const mapDispatchToProps = dispatch => {
  return {
    addContacts: (contacts) => dispatch(postRequest(contacts))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewContact);