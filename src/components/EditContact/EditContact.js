import React, {Component, Fragment} from 'react';
import axios from '../../axios-contact'

class EditContact extends Component {

  state = {
    name: '',
    email: '',
    photo: '',
    phone: ''
  };

  componentDidMount() {
    const id = this.props.match.params.id;
    axios.get(`/contacts/${id}.json`).then(response => {
      this.setState({
        name: response.data.name,
        email: response.data.email,
        photo: response.data.photo,
        phone: response.data.phone
      })
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const id = this.props.match.params.id;
    if(this.state.id !== prevState.id) {
      axios.get(`contacts${id}.json`).then(response => {
        this.setState({
          name: response.data.name,
          email: response.data.email,
          photo: response.data.photo,
          phone: response.data.phone
        })
      })
    }
  }

  changeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  editPage = (event) => {
    const id = this.props.match.params.id;
    event.preventDefault();
    const data = {
      name: this.state.name,
      email: this.state.email,
      photo: this.state.photo,
      phone: this.state.phone
    };
    axios.put(`contacts${id}.json`, data).then(() => {
      this.props.history.replace('/contacts');
    });
  };

  contactsHandler = () => {
    const contacts = {...this.state};


    this.props.addContacts(contacts);
    this.props.history.push('/contacts')
  };

  render() {
    return (
      <Fragment>
        <h2>Edit Dishes</h2>
        <form onSubmit={this.editPage}>
          <div>
            <label htmlFor="Name">Name: </label>
            <input type="text" name='name' id='Name'
                   value={this.state.name}
                   onChange={this.changeHandler}/>
            <label htmlFor="Phone">Phone: </label>
            <input type="text" name='phone' id='phone'
                   value={this.state.phone}
                   onChange={this.changeHandler}/>
            <label htmlFor="Email">Email: </label>
            <input type="email" name='email' id='Email'
                   value={this.state.email}
                   onChange={this.changeHandler}/>
            <label htmlFor="Photo">Photo URL: </label>
            <input type="text" name='photo' id='Photo'
                   value={this.state.photo}
                   onChange={this.changeHandler}/>
            <label htmlFor="PhotoPreview">Photo Preview: </label>
            <button onClick={this.contactsHandler}>Save</button>
          </div>
        </form>
      </Fragment>
    );
  }
}

export default EditContact;