import axios from '../../axios-contact';
export const GET_CONTACTS = "GET_CONTACTS";
export const ADD_CONTACTS = "GET_CONTACTS";

export const addToContact = () => ({type: ADD_CONTACTS});
export const getContactsSuccess = (contacts) => ({type: GET_CONTACTS, contacts});

export const getContacts = () => {
  return (dispatch) => {
    return axios.get('contacts.json').then(response => {
      const contacts = Object.keys(response.data).map(id => {
        return {...response.data[id], id}
      });

      dispatch(getContactsSuccess(contacts));
    })
  }
};

export const  postRequest = contacts => {
  return dispatch => {
    axios.post('contacts.json', contacts).then(() => {
      dispatch(getContacts())
    })
  }
};

export const deleteContact = (id) => {
  return (
    axios.delete('contacts/' + id + '.json').then(() => {
      this.props.history.push('/')
    })
  )
};
