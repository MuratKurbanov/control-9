import React, { Component } from 'react';
import {Route, Switch} from "react-router";
import Layout from "./components/Layout/Layout";
import AddNewContact from "./components/AddNewContact/AddNewContact";
import ContactList from "./components/ContactList/ContactList";
import EditContact from "./components/EditContact/EditContact";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path='/addNewContact' component={AddNewContact} />
          <Route path='/' component={ContactList} />
          <Route path="/editContact/:id" component={EditContact}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
